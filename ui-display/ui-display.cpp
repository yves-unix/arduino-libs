#include "ui-display.hh"
#include "newhaven.h"
#include <stdio.h>

NewHaven_i2c *lcd;

ui_display::ui_display() {

  lcd = new NewHaven_i2c(40);
  Serial.println("ui_display");
  lcd->clearScreen();
  lcd->printChars("Hello", 5);
  // render();
}

void ui_display::assign_var(tMode mode, tPos position, char* name, var8b *var) {

    if(mode != disp_mode1 &&  mode != disp_mode2) {
        return;
    }
  
    if(position != disp_left && position != disp_right) {
        return;
    }

    elems[mode][position].name = name;
    elems[mode][position].var = var;

    render();

    return;
}

void ui_display::set_mode(tMode mode) {

    if(mode != disp_mode1 &&  mode != disp_mode2) {
        return;
    }

    current_mode = mode;

    render();

    return;
}

void ui_display::set_focus (tPos position){

    if(position != disp_left && position != disp_right) {
        return;
    }

    current_position = position;

    render();
}

void ui_display::inc_var_in_focus() {

    (*(elems[current_mode][current_position].var))++;

    render();
}

void ui_display::dec_var_in_focus() {

    (*(elems[current_mode][current_position].var))--;
    render();
}

void ui_display::render() {

   char pl=' ', pr=' ';
   char temp[LCD_MAX_COL+1];

   lcd->clearScreen();
 
   if(current_mode == disp_mode1) {
     if(current_position == disp_left) {
        pl='>';
     } else {
        pr='>';
     }
     sprintf(temp, "%c%3d      %c%5d",
      pl,(unsigned char)*(elems[current_mode][disp_left].var),
      pr,(elems[current_mode][disp_right].var)->get_mvalue());
     strncpy(buffer, temp, LCD_MAX_COL);
     lcd->printCharsAt(buffer, LCD_MAX_COL, LCD_MAX_ROW, LCD_MIN_COL);
   } else {
       sprintf(temp, "--> %3d         ", 
               (elems[current_mode][disp_left].var)->get_value());
      strncpy(buffer, temp, LCD_MAX_COL);
     lcd->printCharsAt(buffer, LCD_MAX_COL, LCD_MAX_ROW, LCD_MIN_COL);
   }
}
