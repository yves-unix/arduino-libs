#ifndef __UI_DISPLAY_H__
#define __UI_DISPLAY_H__

#include "var8b.hh"
#include <newhaven.h>

class ui_display {
  public:
    enum tMode { disp_mode1, disp_mode2 };
    enum tPos { disp_left, disp_right };
    ui_display();
    void assign_var(tMode mode, tPos position, char* name, var8b *var);
    void set_mode(tMode mode);
    void set_focus(tPos position);
    void inc_var_in_focus();
    void dec_var_in_focus();

  #define BUFFER_SIZE (LCD_MAX_COL*LCD_MAX_ROW)

  private:
    struct tUiElem { char *name; var8b *var; };
    struct tUiElem elems[2][2];  // mode - pos
    char buffer[BUFFER_SIZE];
    tMode current_mode = disp_mode1;
    tPos current_position = disp_left;

    //NewHaven_i2c lcd;
    void render();
};

#endif
