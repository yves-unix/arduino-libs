#ifndef __VAR8B_H__
#define __VAR8B_H__


class var8b {

    private:
        unsigned char val;
        unsigned char min_val, max_val;
        unsigned char val_mag;   // multiplication factor

    public:
        var8b(unsigned char init);
        var8b(unsigned char min, unsigned char max, unsigned char mag);
        unsigned char get_value();
        unsigned int  get_mvalue();
        void operator ++() { if(val<max_val) val++ ; } ;
        void operator ++(int) { if(val<max_val) val++ ; } ;
        void operator --() { if(val>min_val) val--; } ;
        void operator --(int) { if(val>min_val) val--; } ;
        void operator=(const var8b& v) { if(this ==&v) ; return ; val=v.val; min_val = v.min_val; }
        operator unsigned char() { return val; };
};
#endif
