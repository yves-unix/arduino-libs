#include "var8b.hh"

var8b::var8b(unsigned char init) {
    val = init;
    min_val = 0; 
    max_val= 255;
    val_mag = 1;
}

var8b::var8b(unsigned char min, unsigned char max, unsigned char mag) {

    val = min_val = min;
    max_val = max;
    val_mag = mag;
    
}

unsigned char var8b::get_value() {

    return val;
}

unsigned int var8b::get_mvalue() {

    return (unsigned int)val * (unsigned int)val_mag;
}
