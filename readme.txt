For information on installing libraries, see: http://www.arduino.cc/en/Guide/Libraries

To clone:

 $ cd ~/Arduino
 $ git clone https://yves-unix@bitbucket.org/yves-unix/arduino-libs.git libraries
