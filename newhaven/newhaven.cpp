#include "newhaven.h"
#include <Wire.h>

#define LCD_COMMAND		0xFE
#define LCD_ON			0x41
#define LCD_OFF			0x42
#define LCD_CLEARSCREEN		0x51
#define LCD_SET_BRIGHTNESS  	0x53
#define LCD_MOVE_CURSOR		0x45

#define LCD_MIN_ROW		0x01
#define LCD_MAX_ROW		0x02
#define LCD_MIN_COL		0x01
#define LCD_MAX_COL		0x10
#define LCD_ROW_SEPARATOR       0x40

#define LCD_CMD_CLEARSCREEN_PAUSE 10  // ms

NewHaven_i2c::NewHaven_i2c() {

    Wire.begin();
    Wire.setClock(50000); 
    delay(1);
    turnDisplayOn();
    setBrightness(2);
    clearScreen();

    return;
}

NewHaven_i2c::NewHaven_i2c(unsigned char i2c_addr) {

    addr = i2c_addr;

    Wire.begin();
    Wire.setClock(50000); 
    return;
}

void NewHaven_i2c::sendCommand(unsigned char cmd, unsigned char* args, 
                               unsigned char len, unsigned char cmd_delay) {

  Wire.beginTransmission(addr);
  Wire.write(LCD_COMMAND);
  Wire.write(cmd);
  if(args != NULL && len != 0) {
     for(unsigned char i=0;i<len;i++) {
         Wire.write(args[i]);
     }
  }  
  Wire.endTransmission();
  delay(cmd_delay);
}

void NewHaven_i2c::sendChars(char* args, unsigned char len) {

  if(args == NULL || len == 0)
      return;

  Wire.beginTransmission(addr);
  for(unsigned char i=0;i<len;i++) {
      Wire.write(args[i]);
  }
  Wire.endTransmission();

  delay(LCD_CMD_DEFLT_PAUSE);
}

void NewHaven_i2c::clearScreen(void) {

    sendCommand(LCD_CLEARSCREEN, NULL, 0, LCD_CMD_CLEARSCREEN_PAUSE);
    
}

int8_t NewHaven_i2c::moveCursorTo(unsigned char col, unsigned char row) {

    unsigned char cur_pos;

    if(col < LCD_MIN_COL || col > LCD_MAX_COL)
        return -1;    
     
    if(row < LCD_MIN_ROW || row > LCD_MAX_ROW)
        return -1;    

    cur_pos=(row - 1)*LCD_ROW_SEPARATOR + (col - 1);
    
    sendCommand(LCD_MOVE_CURSOR, &cur_pos, sizeof(cur_pos));

    return 0;
}

void NewHaven_i2c::moveCursorHome(void) {

    (void)moveCursorTo(LCD_MIN_COL, LCD_MIN_ROW);

}

void NewHaven_i2c::turnDisplayOn(void) {

    sendCommand(LCD_ON,NULL,0);
    sendCommand(LCD_SET_BRIGHTNESS,&brightness,sizeof(brightness));
}

void NewHaven_i2c::turnDisplayOff(void) {

    unsigned char __bright = LCD_MIN_BRIGHTNESS;

    sendCommand(LCD_SET_BRIGHTNESS,&__bright,sizeof(__bright));
    sendCommand(LCD_OFF,NULL,0);
}

int8_t NewHaven_i2c::setBrightness(unsigned char br) { 

    if(br < LCD_MIN_BRIGHTNESS || br > LCD_MAX_BRIGHTNESS)
        return -1;

    sendCommand(LCD_SET_BRIGHTNESS,&br,sizeof(br));

}

void NewHaven_i2c::printChar(char c) {

    if(c == LCD_COMMAND)
        return;

    sendChars(&c, sizeof(c));
}

void NewHaven_i2c::printCharAt(char c, unsigned char row, unsigned char col) {

    if(moveCursorTo(col, row)) 
        return;
    
    printChar(c);
}

void NewHaven_i2c::printChars(char *c, unsigned char len) {

    sendChars(c,len);
}

void NewHaven_i2c::printCharsAt(char *c, unsigned char len, 
                                unsigned char row, unsigned char col) {

    moveCursorTo(col, row);    
    sendChars(c,len);
}

void NewHaven_i2c::printDoubleLine (char twoLines[LCD_MAX_COL*LCD_MAX_ROW]) {

    unsigned char row, col;

    clearScreen();
    moveCursorHome();
    Wire.beginTransmission(addr);
    for(row = 0; row < LCD_MAX_ROW; row++ ) {
       Wire.beginTransmission(addr);
       for(col = 0; col < LCD_MAX_COL; col ++) {
           if(twoLines[row * LCD_ROW_SEPARATOR + col] == '\n'          ||
              twoLines[row * LCD_ROW_SEPARATOR + col] == LCD_COMMAND   ||
              twoLines[row * LCD_ROW_SEPARATOR + col] == (char)0  )
               break;
           Wire.write(twoLines[row * LCD_ROW_SEPARATOR + col]);
       }
       Wire.endTransmission();
       delay(LCD_CMD_DEFLT_PAUSE);
    }
}
