/*
  Newhaven.h - Library to control a New Haven serial display, such as NHD-0216K3Z-NSW-BBW-V3.
  Created by Y. Vandervennet, Aug 2020.
  Released under the terms of LGPL v2.1.
*/

#ifndef __NEW_HAVEN_H_
#define __NEW_HAVEN_H_

#include "Arduino.h"

#define LCD_MIN_BRIGHTNESS      0x01
#define LCD_MAX_BRIGHTNESS      0x08

#define LCD_MIN_ROW             0x01
#define LCD_MAX_ROW             0x02
#define LCD_MIN_COL             0x01
#define LCD_MAX_COL             0x10

#define LCD_CMD_DEFLT_PAUSE        5  // ms

class NewHaven_i2c {

    public:
        NewHaven_i2c(unsigned char i2c_addr);
        NewHaven_i2c();
        int8_t moveCursorTo(unsigned char col, unsigned char row);
        void moveCursorHome(void);

        void turnDisplayOn(void);
        void turnDisplayOff(void);
        int8_t setBrightness(unsigned char br);
        void clearScreen(void);

        void printChar(char c);
        void printCharAt(char c, unsigned char row, unsigned char col);
        void printChars(char *c, unsigned char len);        
        void printCharsAt(char *c, unsigned char len,
                          unsigned char row, unsigned char col);
        void printDoubleLine (char twoLines[LCD_MAX_COL*LCD_MAX_ROW]);

    private:
        unsigned char addr=40;
        unsigned char brightness = 8; 

        void sendCommand(unsigned char cmd, unsigned char* args, unsigned char len, 
                         unsigned char cmd_delay=LCD_CMD_DEFLT_PAUSE);
        void sendChars(char* args, unsigned char len);
};

typedef struct { unsigned char cc[8]; } nhcc_t;

const nhcc_t smiley = { {0x1F, 0x04, 0x1F, 0x15, 0xE, 0x15, 0x1D, 0x1F }};
const nhcc_t lit= { {0xFF, 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF} };

#endif
